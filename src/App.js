import { useState } from 'react';
import './App.css';
import TaskForm from './components/taskForm/TaskForm';
import Tasks from './components/tasks/Tasks';
import tasksJson from './mocks/task.json';


const HelloWorld = (props) => {

  const [show, setShow] = useState(true);

  let renderDom;

  const handleHWButonAction = () => { setShow(!show) };

  if (show) {
    renderDom = <div>
      <h3>{props.subtitle}</h3>
      <div id="hello" > {props.myText}</div>
      <button onClick={handleHWButonAction}>hide information</button>
    </div>
  } else {
    renderDom =
      <div>
        <h3>nothing to show</h3>
        <button onClick={handleHWButonAction}>Show information</button>
      </div>

  }

  return renderDom
}

const App = () => {
  const [tasks, setTask] = useState(tasksJson);

  const addTask = (newTask) => {
    const taskToAdd = {
      ...newTask,
      id: tasks.length +1
    }
    setTask([...tasks, taskToAdd]);
  }

  const deleteTask = id => {
    const newTasks = tasks.filter(task => task.id !== id);
    setTask(newTasks);
  }

  const checkDone = (id) => {
    const newTask = tasks.map(task => {
      if (task.id === id) {
        task.done= !task.done;
      }
      return task;
    })
    setTask(newTask)
  }

  return <div>This is my component:
    <HelloWorld myText="Hello Computer" subtitle="subtitulos" />
    <TaskForm addTask={addTask}/>
    <Tasks tasks={tasks} propdeleteTask={deleteTask} propCheckDone={checkDone}/>
  </div>
};



export default App;
