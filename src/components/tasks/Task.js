const Task = (prop) => {

    const styleCompleted = (taskDone) => {
        return {
            fontSize: '20px',
            color: taskDone ? 'red' : 'gray',
            textDecoration: taskDone ? 'line-through' : 'none'
        }
    }
    const btnDelete = {
        fontSize: '18px',
        background: '#ea2027',
        color: '#fff',
        border: 'none',
        padding: '10px 15px',
        borderRadius: '50%',
        cursor: 'pointer'
    }

    const { task, propdeleteTask, propCheckDone } = prop;
    const { id, title, done } = task;


    const buton = done ? <button onClick={() => propdeleteTask(id)} style={btnDelete}>X</button> : '';
    return <li >
        <p style={styleCompleted(done)}>{title}</p>
        <input type="checkbox"
            onChange={() => propCheckDone(id)}
            checked={done}
        />
        {buton}
    </li>

}

export default Task