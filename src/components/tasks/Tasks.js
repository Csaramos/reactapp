import './Tasks.css';
import Task from "./Task";
import PropTypes from 'prop-types';

const Tasks = (props) => {
    const { tasks, propdeleteTask, propCheckDone } = props;

    Tasks.propTypes = {
        /*         tasks: PropTypes.arrayOf(PropTypes.shape({
                    id: PropTypes.number.isRequired,
                    title: PropTypes.string.isRequired,
                    description: PropTypes.string.isRequired,
                    done: PropTypes.bool.isRequired,
                })).isRequired, */
        tasks: PropTypes.array.isRequired
    }

    return (<div>
        <h1>Tasks:</h1>
        <ul>
            {tasks.map(task =>
                <Task
                    key={task.id}
                    task={task}
                    propdeleteTask={propdeleteTask}
                    propCheckDone={propCheckDone}
                />
            )}
        </ul>
    </div>);
}

export default Tasks;