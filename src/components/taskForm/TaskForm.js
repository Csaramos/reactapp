import { useState } from "react";
import "./TaskForm.css"

const TaskForm = (props) => {
    const taskPrototype =     {
        title: "",
        description: ""
    }
    const [newTask, setNewTask] = useState(taskPrototype);
/*+     const [title, setTittle] = useState('');
    const [description, setDescription] = useState(''); +*/

    const handleSubmit = event => {
        event.preventDefault();
        props.addTask(newTask);
    }

    const handleOnchange = event => {
        const eventTarget= event.target;
        const currentTask = {
            ...newTask
        }
        currentTask[eventTarget.name] = eventTarget.value;
        setNewTask(currentTask);
    }

    return (
        <form onSubmit={handleSubmit}>
            <input
                type="text"
                placeholder="write a task"
                name="title"
                onChange={handleOnchange}
                value={newTask.title} />
            <textarea
                placeholder="Write a description"
                name="description"
                onChange={handleOnchange}
                value={newTask.description} />
{               /* + onChange={e => setDescription(e.target.value)}
                value={description}> +*/ }
            <button type="submit">
                Send information
            </button>
        </form>)
}

export default TaskForm;